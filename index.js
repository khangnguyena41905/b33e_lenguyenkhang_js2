// bài tập 1
var daysEl = document.getElementById("days");
var result1El = document.getElementById("result_1");
var curency1El = document.getElementById("curency");
function handle_1() {
  var sumEx1 = daysEl.value * 100000;
  result1El.innerText = sumEx1;
  curency1El.innerText = "VND";
}
// bài tập 2
// tính giá trị trung bình 5 số
var num1El = document.getElementById("num1");
var num2El = document.getElementById("num2");
var num3El = document.getElementById("num3");
var num4El = document.getElementById("num4");
var num5El = document.getElementById("num5");
var result2El = document.getElementById("result_2");
function handle_2() {
  var sumEx2 =
    (Number(num1El.value) +
      Number(num2El.value) +
      Number(num3El.value) +
      Number(num4El.value) +
      Number(num5El.value)) /
    5;
  console.log("sumEx2: ", sumEx2);
  result2El.innerText = sumEx2;
}
// Bài tập 3
// Quy đổi tiền

var USDEl = document.getElementById("USD");
var result3El = document.getElementById("result_3");
var curency3El = document.getElementById("curency_3");
function handle_3() {
  var VND = USDEl.value * 23500;
  result3El.innerText = VND;
  curency3El.innerText = "VND";
}
// Bài tập 4
// Tính chu vi và diện tích

var daiEl = document.getElementById("dai");
var rongEl = document.getElementById("rong");
var chuViEl = document.getElementById("Chu_vi");
var dienTichEl = document.getElementById("Dien_tich");
function handle_4() {
  var C = (Number(daiEl.value) + Number(rongEl.value)) * 2;
  chuViEl.innerText = C;
  var S = daiEl.value * rongEl.value;
  dienTichEl.innerText = S;
}

// Bài tập 5
// Tính tổng 2 ký số

var rdNumEl = document.getElementById("rdNum");
var result5El = document.getElementById("result_5");
function handle_5() {
  var don_vi = Math.floor(rdNumEl.value % 10);
  var chuc = Math.floor(rdNumEl.value / 10);
  var sumEx5 = Number(chuc) + Number(don_vi);
  result5El.innerText = sumEx5;
}
